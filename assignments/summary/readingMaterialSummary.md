# **INDUSTRIAL INTERNET OF THINGS**

## **WHAT IS IIoT?**

- The Industrial Internet of Things (IIoT) refers to interconnected sensors, instruments, and other devices networked together with computers' industrial applications, including manufacturing and energy management. 

- The IIoT is enabled by technologies such as cybersecurity, cloud computing, edge computing, mobile technologies, machine-to-machine, 3D printing, advanced robotics, big data, internet of things, RFID technology, and cognitive computing.

![IIoT](/uploads/883633f726bfed73d1f8f4e99263975a/IIoT.png)


### **Layered modular architecture IIoT**
1. Content layer - User interface devices (e.g. screens, tablets, smart glasses)

1. Service layer - Applications, software to analyze data and transform it into information

1. Network layer - Communications protocols, wifi, cloud computing

1. Device layer  -  Hardware: CPS, machines, sensors

![Four-layer-architecture-of-IIoT-WSN-M2M-and-CPS](/uploads/ddef20d12caafeef4fd4f9c338b84d99/Four-layer-architecture-of-IIoT-WSN-M2M-and-CPS.png)

### **HISTORY OF INDUSTRY**

![HISTORY](/uploads/b37b01729ca12dfdbf14465f480b9539/HISTORY.PNG)

- 1st industrial revolution – Steam and water power are used to mechanize production

- 2nd industrial revolution – Electricity allows for mass production with assembly lines

- 3rd industrial revolution – IT and computer technology are used to automate processes

- 4th industrial revolution (Industry 4.0) – Enhancing automation and connectivity with CPS


# **INDUSTRY 3.0**

![industry_3.0](/uploads/89ffcf74c038ee9f3d19d04a0dd00104/industry_3.0.png)


### **FIELD DEVICES**



> **Field devices are either sensors or actuators in the factory, either they are measuring or actuating some process. They are  connected in the factory itself.**


### **Control Devices**



> **These are either PC's, PLC's or DCS.These devices control field devices.**


## **Station**



> **Set of field devices and control devices is called station.**


## **Work Centers**



> **Set of stations are work centers, this is like one factory.**


## **Enterprise**



> **Factories together make one enterprise.**

## **INDUSTRY 3.0 ARCHITECTURE**

![ARCHITECTURE_3.0](/uploads/57ad57be325ad0bad98f0a64401e6ce9/ARCHITECTURE_3.0.png)

### **FIELDBUS**



> **Sensors and actuators communicate with each other through fieldbus. These devices are Field Devices**



### **SCADA AND ERP**



> **Scada is on work center level and Erp is on Enterprise level.These are softwares which stores data(files and excel sheets).**


# **INDUSTRY 4.0**
 ![4](/uploads/ab306af0b3efc00f1984e3259b424796/4.PNG)

- Industry 4.0 is the digital transformation of manufacturing/production and related industries and value creation processes. 

- Industry 4.0 is used interchangeably with the fourth industrial revolution and represents a new stage in the organization and control of the industrial value chain.



## **INDUSTRY 4.0 ARCHITECTURE**
![ARCHITECTURE_4.0](/uploads/ba4fdaec3dd674fccdabd2e95bf5c223/ARCHITECTURE_4.0.png)
### **What does Edge Does?**


> **Edge takes data from the controllers and convert it to the protocols that the cloud understands and sends the data from controller to cloud.**


#### **INDUSTRY 4.0 COMMUNICATION PROTOCOLS**

- Mqtt
- CoAP
- AMQP
- HTTP
- WebSockets
- RESTful API



#### **PROBLEMS WITH INDUSTRY 4.0 UPGRADES**

- Cost
- Downtime
- Reliability



#### **CHALLENGES IN CONVERSION INDUSTRY 3.0 TO INDUSTRY 4.0**

- Expensive Hardware
- Lack of documentation
- Properitary PLC protocols


#### **HOW TO MAKE YOUR OWN INDUSTRIAL IOT PRODUCT**

- Identify most popular Industry 3.0 devices
- Study Protocols that these device communicates
- Get Data from Industry 3.0 devices
- Send them to cloud


#### **THE DATA YOU SEND TO THE INTERNET IS STORED IN THE TSDB's**



> **TSDB'S are time series databases i.e the value of your data is stored with respect to time.**


#### **IOT TSDB TOOLS ARE**

- Prometheus
- InfluxDB
- Grafana
- Thingsboard



#### **IOT PLATFORMS**


> **IOT platforms helps to analyze the data, predective maintainence.**


Examples of IOT Platforms

- Google IOT

- AWS IOt

- Azure IOT

- Thingsboard



**GET ALERTS**


> **Monitor TSDB and Trigger Alerts. Zapier and Twilio are tools that helps to raise alerts.**

# **CONVERTING INDUSTY 3.0 DEVICES TO INDUSTRY 4.O DEVICES**

![Interaction-between-hierarchies-Levels-in-Industry-30-and-Industry-40-source](/uploads/d5b8fbda8943cc6600e7ff8adcdc8609/Interaction-between-hierarchies-Levels-in-Industry-30-and-Industry-40-source.png)

- Operational report

- Descriptive analytics

- Diagnostic analysis

- Preditive analysis



# COMPARISON AMONG INDUSTRY 3.0, 3.5 AND 4.0
![Comparisons-among-Industry-30-35-and-40-14](/uploads/5bb4dc33e294ad885f32da4abb4a5d7f/Comparisons-among-Industry-30-35-and-40-14.png)